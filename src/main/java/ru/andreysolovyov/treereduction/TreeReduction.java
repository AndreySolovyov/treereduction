package ru.andreysolovyov.treereduction;

import java.io.IOException;
import java.util.Random;

/**
 * @author USER04
 */
public class TreeReduction {
	/*
	 * ����� � ��������� ���� 3 ������ ����������: ��� ������ ������� (int, 32
	 * ����), ��� float (32 ����, ��������� �����) ��� double (64 ����,
	 * ��������� �����)
	 * 
	 * ��� ������� ���� ���� �� �������, ����� �������� �� �������.
	 */
	int[] intArrayToReduce;
	float[] floatArrayToReduce;
	double[] doubleArrayToReduce;

	/*
	 * ���������� ������� � ��������� �������. ������ ������� ������ �������� �
	 * ������� Runnable. ��� ��� ���� �������� ���������, ��� ���������� �
	 * ������. ��� ������� ��������� ������ ���������������� �������
	 * Runnable-���, ����� �� ����� ������� �� �������� � ������� � ���������
	 * ����� �� ���������� Thread-��. Thread-� ��������� ������ �� Runneable-��
	 * � ��������� �������. �� :)
	 */
	TreeReductiveRunnable[] treeReductives;

	// ��������� ��������� �����. � ��� ������� �� ��������� ������, �������
	// ����� ����������.
	java.util.Random random;

	/*
	 * ���� � ������ ����������. �������������, ���������, ��������.
	 */

	enum CalculationType {
		INTEGER, FLOAT, DOUBLE;
	}

	/*
	 * ����� � ��������� ���� 3 ���� Runnable-��. Runnable ��� ����������� �����
	 * ����� (������ � �������� intArrayToReduce), Runnable ��� �����������
	 * ������� (������ � �������� floatArrayToReduce), Runnable ��� �����������
	 * ������ (������ � �������� doubleArrayToReduce)
	 * 
	 * �� ������ ���������, ������� ������ � ���� ����������, � ������� ��������
	 * �����. ��� ��� ����������� �� ������������ ������ TreeReductiveRunnable
	 */
	abstract class TreeReductiveRunnable implements Runnable {
		/*
		 * ������, �������� �����. � ������ ����� ����� ������ ����������
		 * (startPosition) � ��������� �������� (endPosition) �������,� ��������
		 * �� ������� �� ����������.
		 * 
		 * [startPosition..endPosition) �.�. endPosition �� ������������.
		 * 
		 * ��� ������� ����������� �� ��������� ���������� ������ � �������
		 * �������. ��������, ������ �� 100 ���������, 2 �����. � Runnabl� ���
		 * ������� ����� ����� startPosition = 0, endPosition = 50 ��� �������
		 * startPosition = 50, endPosition = 100
		 */
		int startPosition, endPosition;

		int getStartPosition() {
			return startPosition;
		}

		void setStartPosition(int position) {
			startPosition = position;
		}

		int getEndPosition() {
			return startPosition;
		}

		void setEndPosition(int position) {
			endPosition = position;
		}

		/*
		 * ���������� ����� ���� � ��������� ��������. ������ ����� �������
		 * ������ ����� ������� ���������� �����.
		 */
		abstract void resetSum();
	}

	// Runnable ��� int
	class IntegerTreeReductiveRunnable extends TreeReductiveRunnable {
		int sum;

		@Override
		public void run() {
			for (int i = startPosition; i < endPosition; i++) {
				sum += intArrayToReduce[i];
			}
		}

		void resetSum() {
			sum = 0;
		}

		int getSum() {
			return sum;
		}
	}

	// Runnable ��� float
	class FloatTreeReductiveRunnable extends TreeReductiveRunnable {
		float sum;

		@Override
		public void run() {
			for (int i = startPosition; i < endPosition; i++) {
				sum += floatArrayToReduce[i];
			}
		}

		void resetSum() {
			sum = 0;
		}

		double getSum() {
			return sum;
		}
	}

	// Runnable ��� double
	class DoubleTreeReductiveRunnable extends FloatTreeReductiveRunnable {
		@Override
		public void run() {
			for (int i = startPosition; i < endPosition; i++) {
				sum += doubleArrayToReduce[i];
			}
		}
	}

	public static void main(String[] args) throws IOException {
		TreeReduction t = new TreeReduction();
		/*
		 * ������ ������. ������ �� ������ ��������� ���������.
		 * 
		 * pow = 25 ������ ������� �������������� ��� 2 � ������� pow. ������,
		 * ��������, 2^25 ���������. pow �� ������ ���� ������ 31, ��� ��� ���
		 * ������� ������������ ��������. �������, 2^32 ����� ��� ��� 4 ����.
		 * ������, ���-�� � 25-26 ��� ������ INTEGER ��� FLOAT � ����� �������� � 1�� ��� �������������
		 * ������ � ���������� �������� (�������������� � DOUBLE ������� �� 1 ������).
		 * 
		 * threadCount = 2 ����� �������.
		 * 
		 * iterationCount = 50 ������� ��� �� ������� ����� ������� �� 1 ������.
		 * �����������, �� ����� ������� ������� ������������ �������� ������. �
		 * ����� �������� ���� ����� �������� �����, ���������� �����������
		 * ��������� ��� 10-20-50.
		 * 
		 * runCount = 10 ����� ��������. �.�. �� ��������� �������� 10 ���, �
		 * ������ ���� 50 ��� ������� ����� �������.
		 */
		int pow = 25, threadCount = 2, runCount = 10, iterationCount = 50;

		long seed = 123l; // ��������� ��������� ��� ���������� ��������� �����,
							// ����� �� ���� � ��� ���������� ���������� �������
							// � ����� ���� ����������������� ������.

		try {
			// ��������� ��������� �������� �� ��������� ������.
			// ����� �� ��� ������, ��� ����� ��� ��� � ���� ����������������.
                                                                     // pow  threadCount   runCount  iterationCount
			// �� ���� ���, ������ �����: java TreeReduction 25        2          10          50
			if (args != null & args.length == 4) {
				if (args[0] != null && args[1] != null && args[2] != null
						&& args[3] != null) {
					pow = Integer.parseInt(args[0]);
					threadCount = Integer.parseInt(args[1]);
					runCount = Integer.parseInt(args[2]);
					iterationCount = Integer.parseInt(args[3]);
				}
			}

			// ����������. ������� ������ �.�. �� ������ 30, ����� ������ > 0,
			// ����� �������� � ������� > 1
			if (!((pow >= 0 && pow <= 30) && (threadCount > 0) && (iterationCount > 0) && (runCount > 0))) {
				System.out.println("Args error!\n "
						+ "Valid args are:\n runs: " + "[1..MAX_INT), "
						+ "pow: [0..30], " + "threadCount (0..MAX_INT)");
				return;
			}

			// ��������� �������� � ������ �� ���� ������� ����������.
			t.runBenchmark(seed, pow, threadCount, runCount, iterationCount,
					CalculationType.INTEGER);

			t.runBenchmark(seed, pow, threadCount, runCount, iterationCount,
					CalculationType.FLOAT);

			t.runBenchmark(seed, pow, threadCount, runCount, iterationCount,
					CalculationType.DOUBLE);

			System.out.println("���������� ���������");

		} catch (Exception e) {
			System.out.println("Error!");
			e.printStackTrace();
		}

	}

	private void runBenchmark(long seed, int pow, int threadCount,
			int runCount, int iterationCount, CalculationType type)
			throws Exception {
		// ��������� � �����������: ��������� �������, ������� Runnable-� �
		// ������� ��������� � �.�.
		prepareCalculations(seed, (int) Math.pow(2, pow), threadCount,
				iterationCount, type);

		double sum; // ����� � �������. ����� ����� ���� �� ������ ��������.

		// ��������� �������� runCount ���, � ����� ������ - 10 ���.
		for (int run = 0; run < runCount; run++) {
			sum = 0; // ����� � �������.
			long start = System.currentTimeMillis(); // �������� ����� ������
			// ������������ ����� ������� iterationCount (50) ���.
			for (int iteration = 0; iteration < iterationCount; iteration++) {
				/*
				 * ����� ������ ������� ������
				 */
				sum += calculate(type); // ��� ������������ ���� ��������
										// ������� � ������������ �����.
				/*
				 * ����� ������ ������� ������
				 */
			}
			long end = System.currentTimeMillis(); // �������� ����� �����
			System.out.print("����� = ");

			/*
			 * � ���������� �� ���� ��������� �������, � ������� �� ��������,
			 * �������� ����������� ����� � ������� ����.
			 */
			if (type == CalculationType.INTEGER) {
				System.out.print((long) sum);
			} else {
				System.out.print(sum);
			}
			System.out.print("\n��������� �� " + (end - start)
					+ " �����������\n\n");
		}
	}

	/*
	 * �����, � ������� �� �������������� ������ � ��������.
	 */
	private void prepareCalculations(long seed, int arraySize, int threadCount,
			int iterationCount, CalculationType type) {

		// �������������� ��������� ��������� �����.
		random = new Random(seed);

		// ������ ����������� ������� ���������.
		treeReductives = new TreeReductiveRunnable[threadCount];

		System.out.println("���������� ������...");

		long start = System.currentTimeMillis(); // �������� ����� ������

		switch (type) { // � ����������� �� ���� ����������, ���������� �������
						// ������ ����� �����
		case INTEGER:
			intArrayToReduce = new int[arraySize];// ������ ������

			for (int i = 0; i < arraySize; i++) { // ��������� �������
				// ������ �� -25 �� 25.
				// ��������� ����� ����, ����� �� ����������� ��
				// ���� ������������.
				intArrayToReduce[i] = random.nextInt(50) - 25;
			}
			for (int i = 0; i < threadCount; i++) {
				treeReductives[i] = new IntegerTreeReductiveRunnable();
			}
			break;

		case FLOAT:
			floatArrayToReduce = new float[arraySize];
			for (int i = 0; i < arraySize; i++) {
				floatArrayToReduce[i] = random.nextFloat() * 50 - 25;
			}
			for (int i = 0; i < threadCount; i++) {
				treeReductives[i] = new FloatTreeReductiveRunnable();
			}
			break;

		case DOUBLE:
			doubleArrayToReduce = new double[arraySize];
			for (int i = 0; i < arraySize; i++) {
				doubleArrayToReduce[i] = random.nextDouble() * 50 - 25;
			}
			for (int i = 0; i < threadCount; i++) {
				treeReductives[i] = new DoubleTreeReductiveRunnable();
			}
			break;
		}

		/*
		 * ������������ ������� �� ������� ������� ������� �� ����� ������.
		 * ��������, 10 ���������, 3 ������. ������� 1. ��� ������, ��� ������
		 * ����� ������� ��������� �� 3 ���������, ������ ���� �� 3 ���������,
		 * ������ - �� 3+1 ���������.
		 * 
		 * �.�. ����� ����������� ��� �����, ���������� ����� ������ ����������
		 * ���� ������ ������.
		 */
		int residual = arraySize % threadCount;
		for (int i = 0; i < threadCount; i++) {

			/*
			 * ���� ����� ��������� �� ������� �� ����� ������ �� �����
			 * ��������� ���������� ������� ������ �� ������ �������
			 * 
			 * offset - ��� ���������� ��������� ��� �������� � ������ ������.
			 * ��������� �������� � ������ - ��� ������ + offset
			 * 
			 * ������ ������� ������� ������ - �������.
			 */
			// ���� ���� ���� � ��� �� ��������� ? ��, �� ���������. ����� �����
			// ������ = arraySize / threadCount
			int offset = (i != threadCount - 1 ? arraySize / threadCount
			// ���, ���������. ����� ����� = arraySize / threadCount + �������
					: arraySize / threadCount + residual);

			if (i != 0) {
				/*
				 * ��� runnable-��, ������� ������ ������ ��� ������� ������� ��
				 * �������, ������������� ������ ������ ��� �������� ������
				 * ����������� runnable.
				 */
				treeReductives[i].startPosition = treeReductives[i - 1].endPosition;
			}
			// ������ ����� ��� ��������� ������ + �����
			treeReductives[i].endPosition = treeReductives[i].startPosition
					+ offset;
		}
		long end = System.currentTimeMillis(); // �������� ����� �����

		System.out.println("������ ������������ �� " + (end - start)
				+ " �����������.\n" + "�������� ����������. ������ ������� "
				+ arraySize + ", ����� ������� " + threadCount + ", seed "
				+ seed + ", ����� �������� " + iterationCount
				+ ", ��� ���������� " + type);
	}

	// ������������� �����. ������� ����� ���� ��� ������� runnable �� �������
	// ���������.
	private Thread[] prepareThreads() {
		int length = treeReductives.length;

		Thread[] threads = new Thread[length];

		// ������� ����� ������ �� ��� ��������� ��������.
		// ������ ���������� �� ����� � ��.
		for (int i = 0; i < length; i++) {
			treeReductives[i].resetSum();
			threads[i] = new Thread(treeReductives[i]);
		}

		return threads;
	}

	/*
	 * �������� ���� ����������. ����� ��� ������� ����� ����� ��������.
	 */
	private double calculate(CalculationType type) throws Exception {
		// C���� �� ����� ��������. ��� �������� ��� ����, �.�. ���� �� ��� ���
		// �������� ��
		// ������������ ��������. ���, �����, ����. ������ ���������������.
		double sum = 0;

		Thread[] threads = prepareThreads();

		int length = threads.length;

		// ��������� �����.
		for (int i = 0; i < length; i++) {
			threads[i].start();
		}

		// ����, ����� ��� ����������.
		for (int i = 0; i < length; i++) {
			threads[i].join();
		}

		// ������ ������ �������� � ���� ����� ������ ����� �������.
		// �������� �� ������� ��������, �������� � ���������� ��� �����.
		for (int i = 0; i < length; i++) {
			switch (type) {
			case INTEGER:
				sum += ((IntegerTreeReductiveRunnable) treeReductives[i])
						.getSum();
				break;

			case FLOAT:
				sum += ((FloatTreeReductiveRunnable) treeReductives[i])
						.getSum();
				break;

			case DOUBLE:
				sum += ((DoubleTreeReductiveRunnable) treeReductives[i])
						.getSum();
				break;
			}
		}
		return sum;
	}
}

/*
 * ��� ����� ��������� ��� core i5 2.3 GHz 2 ����, 4 ����.
 * 
 * 
 * ���������� ������... ������ ������������ �� 665 �����������. ��������
 * ����������. ������ ������� 33554432, ����� ������� 2, seed 123, �����
 * �������� 50, ��� ���������� INTEGER ����� = -838518900 ��������� �� 762
 * �����������
 * 
 * ����� = -838518900 ��������� �� 811 �����������
 * 
 * ����� = -838518900 ��������� �� 768 �����������
 * 
 * ����� = -838518900 ��������� �� 756 �����������
 * 
 * ����� = -838518900 ��������� �� 749 �����������
 * 
 * ����� = -838518900 ��������� �� 749 �����������
 * 
 * ����� = -838518900 ��������� �� 743 �����������
 * 
 * ����� = -838518900 ��������� �� 753 �����������
 * 
 * ����� = -838518900 ��������� �� 748 �����������
 * 
 * ����� = -838518900 ��������� �� 752 �����������
 * 
 * ���������� ������... ������ ������������ �� 652 �����������. ��������
 * ����������. ������ ������� 33554432, ����� ������� 2, seed 123, �����
 * �������� 50, ��� ���������� FLOAT ����� = -945808.88671875 ��������� �� 1245
 * �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1244 �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1245 �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1242 �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1241 �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1243 �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1244 �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1242 �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1243 �����������
 * 
 * ����� = -945808.88671875 ��������� �� 1242 �����������
 * 
 * ���������� ������... ������ ������������ �� 1292 �����������. ��������
 * ����������. ������ ������� 33554432, ����� ������� 2, seed 123, �����
 * �������� 50, ��� ���������� DOUBLE ����� = 2483128.90625 ��������� �� 3459
 * �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3457 �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3454 �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3454 �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3459 �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3462 �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3458 �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3457 �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3456 �����������
 * 
 * ����� = 2483128.90625 ��������� �� 3488 �����������
 * 
 * ���������� ���������
 */
